# Kali NetHunter Utility Package

**This repository has been moved to [gitlab.com/kalilinux/packages/nethunter-utils](https://gitlab.com/kalilinux/packages/nethunter-utils)**.

[nethunter-utils](https://pkg.kali.org/pkg/nethunter-utils) includes various custom scripts for the [Kali NetHunter filesystem/chroot](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-rootfs).
